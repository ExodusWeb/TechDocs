---
layout: default
title: "System Documentation (Pioneer LaserActive)"
pageGroup: "PioneerLaserActive"
---

# System Manuals

&nbsp;&nbsp;&nbsp;Date/Code&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Document&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Author | Description
--- | --- | --- | ---
DRB1141 | {% include download title="Pioneer CLD-A100 - Operating Instructions" target="Manuals/Pioneer CLD-A100 - Operating Instructions - [DRB1141].pdf" %} | Pioneer | 
ARP2757 | {% include download title="Pioneer CLD-A100 - Service Manual" target="Manuals/Pioneer CLD-A100 - Service Manual - [ARP2757].pdf" %} | Pioneer | An official, complete, comprehensive service manual for the CLD-A100 player, including schematics, parts lists, calibration steps, troubleshooting guides, and a logical description of how the player operates.
| {% include download title="Pioneer CLD-A100 - Service Information Bulletins" target="Manuals/Pioneer CLD-A100 - Service Information Bulletins.pdf" %} | Pioneer | This is a compilation of various supplementary service bulletins and other service materials. It was not published as a single document, this is simply a compilaton of a material gathered from an individual repair centre where this information was obtained from.
 | {% include download title="Pioneer PAC-K1 - Operating_Instructions" target="Manuals/Pioneer PAC-K1 - Operating_Instructions.pdf" %} | Pioneer | 
 | {% include download title="Pioneer PAC-N10 - Operating_Instructions" target="Manuals/Pioneer PAC-N10 - Operating_Instructions.pdf" %} | Pioneer | 
ARP2763 | {% include download title="Pioneer PAC-N10 - Service Manual" target="Manuals/Pioneer PAC-N10 - Service Manual - [ARP2763].pdf" %} | Pioneer | 
 | {% include download title="Pioneer PAC-S10 - Operating_Instructions" target="Manuals/Pioneer PAC-S10 - Operating_Instructions.pdf" %} | Pioneer | 
ARP2779 | {% include download title="Pioneer PAC-S10 - Service Manual" target="Manuals/Pioneer PAC-S10 - Service Manual - [ARP2779].pdf" %} | Pioneer | 
RRV1012 | {% include download title="Pioneer GOL-1 - Service Manual" target="Manuals/Pioneer GOL-1 - Service Manual - [RRV1012].pdf" %} | Pioneer | 


# Other Player Manuals

This is a collection of player manuals and service materials for other Laserdisc players other than the CLD-A100. This will be helpful if you are using one of these players for creating RF Laserdisc backups, as well as for a technical reference on player operations and commands. The programming manuals for these players in particular contain details on player states and system commands, which have overlap in some cases with the registers and commands in the low-level register interface provided to the PAC modules. See the [Dragon's Lair Project](https://www.dragons-lair-project.com/tech/ldguide/pioneer.asp) for a more complete compilation of materials across a broader range of manufacturers. Documents lited here will be limited to those of interest.

&nbsp;&nbsp;&nbsp;Date/Code&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Document&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Author | Description
--- | --- | --- | ---
DRE1006 1991 | {% include download title="Pioneer LD-V4300D - Operating Instructions" target="Manuals/Pioneer-LD-V4300D-Operating-Instructions-EN.pdf" %} | Pioneer | 
ARP2311 1991-07 | {% include download title="Pioneer LD-V4300D - Service Manual" target="Manuals/Pioneer-LD-V4300D-Service-Manual-DD86_2.pdf" %} | Pioneer | {% detailsStart Alternate Versions %}{% include download title="Pioneer LD-V4300D - Service Manual" target="Manuals/Pioneer_LD-V4300D_Service_Manual.pdf" %} - Lower quality scan of the manual in black and white rather than colour. {% detailsEnd %}
 | {% include download title="Pioneer LD-V4300D - Brochure with Specifications" target="Manuals/Pioneer_LD-V4300D_Brochure_Specifications.pdf" %} | Pioneer | 
TP116 V1.1 1993-09 | {% include download title="Pioneer LD-V4400 - Level I & III User's Manual - Programmer's Reference Guide" target="Manuals/Pioneer_LD-V4400_Programmers_Reference_Guide.pdf" %} | Pioneer | Since the LD-V4400 is an NTSC-only variation of the LD-V300D, this manual is also applicable for the LD-4300D.
ARP1758 1989-05 | {% include download title="Pioneer LD-V8000 - Service Manual" target="Manuals/Pioneer_LD-V8000_Service_Manual.pdf" %} | Pioneer | 
ARP1759 1989-04 | {% include download title="Pioneer LD-V8000 - Service Guide" target="Manuals/Pioneer_LD-V8000_Service_Guide.pdf" %} | Pioneer | 
TP113 V2.1 1993-02 | {% include download title="Pioneer LD-V8000 - Level I & III User's Manual - Programmer's Reference Guide" target="Manuals/Pioneer_LD-V8000_Level_I_III_Programmers_Reference_Guide.pdf" %} | Pioneer | 
DTP114 V1.0 1991-07 | {% include download title="Pioneer LD-V8000 - Level II User's Manual - Programmer's Reference Guide" target="Manuals/Pioneer_LD-V8000_Level_II_Programmers_Reference_Guide.pdf" %} | Pioneer | 
 | {% include download title="Pioneer LD-V8000 - Brochure with Specifications" target="Manuals/Pioneer_LD-V8000_Brochure_Specifications.pdf" %} | Pioneer | 
 | {% include download title="Pioneer LD-V8000 - Product Information Bulletin - Specifications" target="Manuals/Pioneer_LD-V8000_Product_Information_Bulletin_Specifications.pdf" %} | Pioneer | 
 | {% include download title="Pioneer CLD-V2400 - Brochure with Specifications" target="Manuals/Pioneer_CLD-V2400_Brochure_Specifications.pdf" %} | Pioneer | 
ARP2778 1993-05 | {% include download title="Pioneer CLD-V2600 - Service Manual" target="Manuals/Pioneer_CLD-V2600_Service_Manual.pdf" %} | Pioneer | 
TP117 V2.0 1993-12 | {% include download title="Pioneer CLD-V2600 and CLD-V2400 - Level I & III User's Manual - Programmer's Reference Guide" target="Manuals/Pioneer_CLD-V2600_and_CLD-V2400_Programmers_Reference_Guide.pdf" %} | Pioneer | 
 | {% include download title="Pioneer CLD-V2600 - Brochure with Specifications" target="Manuals/Pioneer_CLD-V2600_Brochure_Specifications.pdf" %} | Pioneer | 
TP120 V1.0 1995-10 | {% include download title="Pioneer CLD-V2800 - Level III User's Manual - Programmer's Reference Guide" target="Manuals/Pioneer_CLD-V2800_Programmers_Reference_Guide.pdf" %} | Pioneer | 
 | {% include download title="Pioneer CLD-V2800 - Brochure with Specifications" target="Manuals/Pioneer_CLD-V2800_Brochure_Specifications.pdf" %} | Pioneer | 
TP121 V1.1 1996-03 | {% include download title="Pioneer CLD-V5000 - Level III User's Manual - Programmer's Reference Guide" target="Manuals/Pioneer_CLD-V5000_Programmers_Reference_Guide.pdf" %} | Pioneer | 


# Standards Documents

Two main sets of standards cover the LaserDisc format, and by extension the LaserActive system - the "Laser vision" IEC standards for PAL/NTSC and their amendments, and for the digital audio/data portion, the [Rainbow Books](https://en.wikipedia.org/wiki/Rainbow_Books), specifically the Red Book and Yellow Book. Additionally there is the CD-V variant, which is a hybrid CD and LaserDisc format. I would love nothing more than to post the full IEC standards documents here, as the documents themselves contain vital information to understanding how these systems work, and in particular how to decode the data. Unfortunately, although these standards are thoroughly obsolete, standards bodies can be aggressive at enforcing copyright, no matter how irrelivent or obsolete it may be to industry. These documents are still available to purchase by the publishing standards bodies, as long as you have deep pockets. That means they cannot reasonably be construed to be abandoned. The relevant standards documents will be listed here, however download links will only be provided to documents externally hosted on archive.org, as we consider they have already obtained permission or qualified for an exemption allowing them to host these materials. Where a document is known but not currenly available on archive.org, the name will appear with no link provided.

&nbsp;&nbsp;&nbsp;Date/Code&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Document&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Author | Description
--- | --- | --- | ---
IEC 60857:1986 1986-10 | [Pre-recorded optical reflective videodisk system "Laser vision" 60 Hz/525 lines - M/NTSC](https://archive.org/download/iec-60857-1986-laservision-ntsc/IEC 60857-1986 Laservision NTSC.pdf) | IEC | 
IEC 60857:1986 1991-07 | [Pre-recorded optical reflective videodisk system "Laser vision" 60 Hz/525 lines - M/NTSC - Amendment 1](https://archive.org/download/iec-60857-1986-laservision-ntsc/IEC 60857-1986 Laservision NTSC Amendment 1.pdf) | IEC | 
IEC 60857:1986 1997-04 | [Pre-recorded optical reflective videodisk system "Laser vision" 60 Hz/525 lines - M/NTSC - Amendment 2](https://archive.org/download/iec-60857-1986-laservision-ntsc/IEC 60857-1986 Laservision NTSC Amendment 2.pdf) | IEC | 
IEC 60856:1986 1986-10 | [Pre-recorded optical reflective videodisk system "Laser vision" 50 Hz/625 lines - PAL](https://archive.org/download/iec-60856-1986-laservision-pal/IEC 60856-1986 Laservision PAL.pdf) | IEC | 
IEC 60856:1986 1991-07 | [Pre-recorded optical reflective videodisk system "Laser vision" 60 Hz/525 lines - PAL - Amendment 1](https://archive.org/download/iec-60856-1986-laservision-pal/IEC 60856-1986 Laservision PAL Amendment 1.pdf) | IEC | 
IEC 60856:1986 1997-05 | [Pre-recorded optical reflective videodisk system "Laser vision" 60 Hz/525 lines - PAL - Amendment 2](https://archive.org/download/iec-60856-1986-laservision-pal/IEC 60856-1986 Laservision PAL Amendment 2.pdf) | IEC | 
IEC 61104:1992 1992-06 | [Compact disc video system — 12cm CD-V](https://archive.org/download/iec-61104-1992-compact-disc-video-system-12cm-cd-v/IEC 61104-1992 Compact Disc Video System 12 cm CD-V.pdf) | IEC | 
IEC 60908:1987 1987-09 | Compact disc digital audio system (Red Book) | IEC | 
IEC 60908:1987 1992-09 | Compact disc digital audio system (Red Book) - Amendment 1 | IEC | 
IEC 60908:1999 1999-02 | [Compact disc digital audio system (Red Book) - Second Edition](https://archive.org/download/red-book-audio-recording-compact-disc-digital-audio-system-iec-60908-second-edit/Red Book - Audio Recording Compact Disc Digital Audio System, IEC 60908, Second Edition, 1999-02 [ISBN 2-8318-4638-2].pdf) | IEC | 
IEC 10149:1989 1989-09 | [Information technology - Data interchange on read-only 120 mm Optical data disks (CD-ROM) (Yellow Book)](https://archive.org/download/iec-10149-1989-data-interchange-on-read-only-120mm-optical-data-discs-cd-rom-yellow-book/IEC 10149-1989 - Data Interchange on read-only 120mm optical data discs (CD-ROM) (Yellow Book).pdf) | IEC | 
IEC 10149:1995 1995-07 | [Information technology - Data interchange on read-only 120 mm Optical data disks (CD-ROM) (Yellow Book) - Second Edition](https://archive.org/download/Isoiec10149ed2.0en/isoiec10149{ed2.0}en.pdf) | IEC | 


# Technical Reference Materials

&nbsp;&nbsp;&nbsp;Date/Code&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Document&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Author | Description
--- | --- | --- | ---
No 1 | {% include download title="Pioneer Tuning Fork - No 1" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.1 (~1980).pdf" %} | Pioneer | Some discussion of FM techniques, but not much relevant for Laserdiscs. Included here for completeness.
1980-04 | {% include download title="Pioneer Tuning Fork - Digital Supplement" target="Documents/Tuning Fork/Pioneer - Tuning Fork - Digital - Supplement (~1980).pdf" %} | Pioneer | Digital electronics primer. Included here for completeness.
No 2 | {% include download title="Pioneer Tuning Fork - No 2" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.2 (~1980).pdf" %} | Pioneer | Not much relevant for Laserdiscs. Included here for completeness.
1984-03 | {% include download title="Pioneer Tuning Fork - No 2 Supplement" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.2 (supplement) (~1980).pdf" %} | Pioneer | Not much relevant for Laserdiscs. Included here for completeness.
No 3 | {% include download title="Pioneer Tuning Fork - No 3" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.3 (~1980).pdf" %} | Pioneer | Not much relevant for Laserdiscs. Included here for completeness.
No 4 | {% include download title="Pioneer Tuning Fork - No 4" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.4 (~1980).pdf" %} | Pioneer | Not much relevant for Laserdiscs. Included here for completeness.
No 5 | {% include download title="Pioneer Tuning Fork - No 5" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.5 (~1980).pdf" %} | Pioneer | Not much relevant for Laserdiscs. Included here for completeness.
No 6 | {% include download title="Pioneer Tuning Fork - No 6" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.6 (~1980).pdf" %} | Pioneer | Thorough introduction into the Laserdisc format, and the principles and technology underpinning Laserdisc players.
No 7 | {% include download title="Pioneer Tuning Fork - No 7" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.7 (~1980).pdf" %} | Pioneer | Thorough introduction into the CD format, and the principles and technology underpinning CD players.
No 8 | {% include download title="Pioneer Tuning Fork - No 8" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.8 (~1980).pdf" %} | Pioneer | Brief introduction to NTSC and PAL, and thorough introduction to NTSC VHS, and the principles and technology underpinning VHS players.
No 9 | {% include download title="Pioneer Tuning Fork - No 9" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.9 (~1980).pdf" %} | Pioneer | Diagnostic and calibration information for servicing LD and CD players. A thorough discussion of the Z80. Coverage of PAL VHS principles and technology.
No 10 | {% include download title="Pioneer Tuning Fork - No 10" target="Documents/Tuning Fork/Pioneer - Tuning Fork No.10 (1992).pdf" %} | Pioneer | Not much relevant for Laserdiscs. Included here for completeness.
1984-11 | {% include download title="Digital Audio Modulation in the PAL and NTSC Optical Video Disk Coding Formats" target="Documents/Digital Audio Modulation in the PAL and NTSC Optical Video Disk Coding Formats.pdf" %} | Philips | Engineering paper proposing the addition of digital audio to the Laserdisc format, which was ultimately carried out as described. It gives a good breakdown of the approach used.
1989 | {% include download title="Basic Laser Disc Technology Guide - 1989" target="Documents/LD_Basic_Tech_Guide_1989.pdf" %} | Pioneer | A good grounding of the Laserdisc format and operation of Laserdisc players
1990-05 | {% include download title="Basic Laser Disc Technology Guide - 1990" target="Documents/LD_Basic_Tech_Guide_1990.pdf" %} | Pioneer | Adds a description of CD-V and describes "trick play" techniques such as reverse, slow-motion, and still frame.

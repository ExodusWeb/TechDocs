---
layout: default
title: LaserActive Overview
pageGroup: "PioneerLaserActive"
---

**Note - This page is a Work in Progress! More detail will be added over time.**

Refer to [SegaRetro](https://segaretro.org/LaserActive) and [Wikipedia](https://en.wikipedia.org/wiki/LaserActive) for a general overview of the LaserActive.

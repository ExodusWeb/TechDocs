---
layout: default
title: "Development Tools/Software (Pioneer LaserActive)"
pageGroup: "PioneerLaserActive"
---

# Development Tools/Software

# Original Tools

{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Images&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Name | Creator | Description
--- | --- | --- | ---
 | {% include download title="PAC-PC1 Program Editor" target="Software/LaserActive_PAC_PC-1_Program_Editor_DOS_and_PC98.zip" %} | Pioneer | 

</div>


# Community Tools

{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Images&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Name | Creator | Description
--- | --- | --- | ---
 | {% include download title="DumpMegaLD" target="Software/DumpMegaLD.zip" %} | Nemesis | A Mode 1 MegaCD ROM with source, capable of initializing the Laserdisc hardware from the PAC-S1/S10 device, and performing a rip of the digital content of a MegaLD/LDROM2 disc over the second controller port. Full raw sector data and subcode extraction supported. This is obsolete with full RF capture and decoding techniques.
 | {% include download title="MegaLDRegEditor" target="Software/DumpMegaLD.zip" %} | Nemesis | A Mode 1 MegaCD ROM with source, capable of initializing the Laserdisc hardware from the PAC-S1/S10 device, and allowing full interactive reading/writing to the PD6103A IC register block, which is what bridges the MegaCD hardware in the PAC-S1/S10 with the LaserActive system. The presence of this chip and the features it provides is the only thing that makes the PAC-S1/S10 meaningfully different from a standalone Mega Drive with a MegaCD attached.
 | {% include download title="LDBIOS.INC" target="Software/LDBIOS.INC" %} | Nemesis | Reverse-engineered BIOS routine entry points for the MegaLD bios on the PAC-S1/S10, in the same format and style as the CDBIOS.INC file from the official MegaCD SDK, however this is not an official file provided by Sega. In addition to the BIOS routines, the header contains detailed notes on efforts to reverse-engineer the functionality of the PD6103A IC register block. The information contained in this document is sufficient to make an attempt at emulating the LaserActive hardware, using a working MegaCD emulator as a base. This file would enable the development of custom MegaLD titles, as well as assist with the reverse-engineering of the LaserActive bios and LaserActive games.

</div>

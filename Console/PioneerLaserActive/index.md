---
layout: default
title: "Pioneer LaserActive"
pageGroup: "PioneerLaserActive"
---

# Overview
For general information about this system and its games, please refer to other sources such as [SegaRetro](https://segaretro.org/LaserActive) and [Wikipedia](https://en.wikipedia.org/wiki/LaserActive). This site focuses on the technical details of the system that are important for emulation and development, and assumes a familiarity with the general and historical facts of the console.

This site also provides information on how to correctly archive LaserActive Laserdiscs, so that they can be preserved from destruction due to laser rot and damage. Additionally, information is given on how to process these captured disc images to extract their analog and digital contents. This information can be applied to other Laserdiscs as well, both those used for games and for general audio/video content. External links to archived content is also made available for all currently known digital images that have been created for the LaserActive, both in their raw, analog RF form, as well as in the form of processed images, video, audio, and software.

# Sections
{% include contents pageGroup=page.pageGroup %}

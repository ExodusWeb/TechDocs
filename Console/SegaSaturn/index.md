---
layout: default
title: "Sega Saturn"
pageGroup: "SegaSaturn"
---

# Overview
The Sega Saturn is the follow-up to Sega's very successful Mega Drive (Genesis), however the Saturn saw mixed results, being popular in Japan but largely a commercial failure in other markets. For general information about this system and its games, please refer to other sources such as [segaretro.org](https://segaretro.org/Sega_Saturn). This site focuses on the technical details of the system that are important for emulation and development, and assumes a familiarity with the general and historical facts of the console.

# System Pictures
{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}
[![](Pictures/SegaSaturn.jpg)](Pictures/SegaSaturn.jpg)
</div>

# Sections
{% include contents pageGroup=page.pageGroup %}

<br><br><br><br><br><br>

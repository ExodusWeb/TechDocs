---
layout: default
title: "Sega Mega Drive (Genesis)"
pageGroup: "SegaMegaDrive"
---

# Overview
The Sega Mega Drive (known as Genesis in North America) is Sega's most successful video game console. For general information about this system and its games, please refer to other sources such as [segaretro.org](https://segaretro.org/Sega_Mega_Drive). This site focuses on the technical details of the system that are important for emulation and development, and assumes a familiarity with the general and historical facts of the console.
# System Pictures
{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}
[![](Pictures/MegaDrive1.jpg)](Pictures/MegaDrive1.jpg)[![](Pictures/MegaDrive2.jpg)](Pictures/MegaDrive2.jpg)
</div>

# Sections
{% include contents pageGroup=page.pageGroup %}

<br><br><br><br><br><br>

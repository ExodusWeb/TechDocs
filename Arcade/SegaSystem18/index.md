---
layout: default
title: "Sega System 18"
---

# Overview
Refer to [SegaRetro](https://segaretro.org/Sega_System_18) and [System16](http://www.system16.com/hardware.php?id=702) for a general hardware overview and list of games.

# System Pictures
{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}
[![](Pictures/ReferenceImage.jpg)](Pictures/ReferenceImage.jpg)
</div>

# Documentation

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;Document&nbsp;&nbsp;&nbsp;&nbsp; | Author | Description | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
--- | --- | --- | --- | ---
| {% include download title="System 18 Schematics (Partial)" target="Schematics/System 18 Schematics (Partial).pdf" %} | Sega | A single page from the official schematics from Sega. Unfortunately only the last page (page 11) has been located, which contains the connector pinouts. The schematics are clean and very easy to read. | Original document
| {% include download title="System 18 Schematics (Jotego)" target="Schematics/System 18 Schematics (Jotego).pdf" %} | Jotego | Reverse engineered schematics done by Jotego. As this was done prior to the discovery of the page above of the official schematics, line names won't match. May contain errors or incomplete information, but a great reference in absense of official schematics. | [GitHub](https://github.com/jotego/jtbin/blob/master/sch/s18.pdf)
420-0010U | {% include download title="Shadow Dancer Installation and Owner's Manual" target="Manuals/420-0010U - Shadow Dancer - Installation and Owner's Manual.pdf" %} | Sega | Installation and owner's manual for Shadow Dancer, with wiring diagram and dipswitch settings. | 
420-5954-01 | {% include download title="Laser Ghost Owner's Manual" target="Manuals/420-5954-01 - Laser Ghost - Owner's Manual.pdf" %} | Sega | Installation and owner's manual for Laser Ghost, with wiring diagram and dipswitch settings. | 
999-0017 | {% include download title="Alien Storm Owner's Manual" target="Manuals/999-0017 - Alien Storm - Owner's Manual.pdf" %} | Sega | Owner's manual for Alien Storm, with wiring diagram and dipswitch settings. | 
999-0038 | {% include download title="Moonwalker Owner's Manual" target="Manuals/999-0038 - Moonwalker - Owner's Manual.pdf" %} | Sega | Owner's manual for Moonwalker, with wiring diagram and dipswitch settings. | 
999-0079 | {% include download title="Clutch Hitter Owner's Manual" target="Manuals/999-0079 - Clutch Hitter - Installation and Owner's Manual.pdf" %} | Sega | Installation and owner's manual for Clutch Hitter, with wiring diagram and dipswitch settings. | 
999-0090 | {% include download title="DD Crew Owner's Manual" target="Manuals/999-0090 - DD Crew - Installation and Owner's Manual.pdf" %} | Sega | Installation and owner's manual for DD Crew, with wiring diagram and dipswitch settings. | 

# High Resolution Photos
##TODO##

# Miscellaneous Photos
Here's a collection of images of this system that have been posted online from various sources: {% include folder title="Hardware Photos" target="Pictures" %}

<br><br><br><br><br><br>

---
layout: default
title: "Sega System 1/2"
---

# Overview
Refer to SegaRetro ([System 1](https://segaretro.org/Sega_System_1), [System 2](https://segaretro.org/Sega_System_2)) and System16 ([System 1](http://www.system16.com/hardware.php?id=693), [System 2](http://www.system16.com/hardware.php?id=694)) for a general overview of the Sega System 1/2 and a list of games.

# System Pictures
{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}
[![](Pictures/ReferenceImage1.jpg)](Pictures/ReferenceImage1.jpg)
[![](Pictures/ReferenceImage2.jpg)](Pictures/ReferenceImage2.jpg)
[![](Pictures/ReferenceImage3.jpg)](Pictures/ReferenceImage3.jpg)
[![](Pictures/ReferenceImage4.jpg)](Pictures/ReferenceImage4.jpg)
</div>

# Hardware Revisions
It's worth noting first that I don't believe the naming that's currently applied to this hardware is correct. The terms "System 1" and "System 2" (and occasionally "System 8"), which are often used online seem entirely made up by the emulation community, with no markings on the boards themselves identifying them in this way, nor the game flyers or manuals, or the schematics. While it's possible that Sega officially named this hardware retrospectively, which has known to have occurred for some later hardware platforms, the general information that's commonly published providing system specifications for this hardware seems unsupported by actual references, and the division between System 1 and 2 games seems confusing and in some cases contradictory. The often quoted statement that the "System 2" added a ROM board is demonstratively false, with the circulated image to support this claim simply being the "Shooting Master" game, which added a daughter board by socketing various main board ICs from a 171-5303 board and relocating them onto the daughter board, with additional hardware for the gun interface.

One thing that is correct is that there is one significant hardware change which affects software compatibility, namely the addition of the "315-5049" tilemap hardware. Games that didn't make use of this hardware can potentially run on any of the boards, possibly with adjustments to account for differences in supported ROM sizes and the like. A variety of official daughter boards were produced by Sega to make older board revisions compatible with newer games which required adjustments in these areas. With the use of these daughter boards, it's likely any game could be adapted to run on any board, unless a dependency on the "315-5049" chip restricted them to "171-5303" board revisions. The best example of this is WonderBoy, which is documented in its manual, and can be seen from pictures, to be running across all the hardware from the earliest board revision to the latest one.

In addition to the games generally accepted as part of the "System 1" hardware line, there's the question of the "Super Locomotive" hardware. Super Locomotive was released in 1982, one year before "Star Jacker" which is generally considered the first System 1 game. Super Locomotive ran on a hardware platform which was virtually identical in terms of functionality to Star Jacker, but with a unique board that had an alternate memory layout. Star Jacker increased the size of some memory regions and shuffled the layout to accommodate the changes, which makes the Star Jacker code, and all following System 1 games, incompatible with the Super Locomotive board layout. Despite the software incompatibility however, this is a fairly minor change in terms of the actual design of the system, and arguably Super Locomotive should be considered to also be running on the System 1 platform.

The following PCBs are known to exist for System 1 hardware:

{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Images&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Part &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Description
--- | --- | ---
[![](Pictures/PCB171-5019Top.jpg)](Pictures/PCB171-5019Top.jpg)[![](Pictures/PCB171-5019Bottom.jpg)](Pictures/PCB171-5019Bottom.jpg) | 171-5019 | Main board. Only used by Super Locomotive. Differences in the memory map make this board hardware incompatible with other System 1 titles. Part number silkscreened on the rear solder side of the board.
[![](Pictures/PCB171-5034Top.jpg)](Pictures/PCB171-5034Top.jpg)[![](Pictures/PCB171-5034Bottom.jpg)](Pictures/PCB171-5034Bottom.jpg) | 171-5034 | Main board. Part number silkscreened on one edge of the solder side of the board.
[![](Pictures/PCB171-5054Top.jpg)](Pictures/PCB171-5054Top.jpg)[![](Pictures/PCB171-5054Bottom.jpg)](Pictures/PCB171-5054Bottom.jpg) | 171-5054 | Main board. Minor layout changes from 171-5034, most evident around the audio CPU. Part number is most likely silkscreened on one edge of the parts side of the board as per 171-5054-02, but unfortunately this is usually covered by a sticker giving the assembled game part number.
[![](Pictures/PCB171-5054-01Top.jpg)](Pictures/PCB171-5054-01Top.jpg)[![](Pictures/PCB171-5054-01Bottom.jpg)](Pictures/PCB171-5054-01Bottom.jpg) | 171-5054-01 | Main board. Very minor layout changes from 171-5054, most evident around the power LED. Part number is most likely silkscreened on one edge of the parts side of the board as per 171-5054-02, but unfortunately this is usually covered by a sticker giving the assembled game part number.
[![](Pictures/PCB171-5054-02Top.jpg)](Pictures/PCB171-5054-02Top.jpg) | 171-5054-02 | Main board. Part number silkscreened on one edge of the parts side of the board.
[![](Pictures/PCB171-5054-03Top.jpg)](Pictures/PCB171-5054-03Top.jpg) | 171-5054-03 | Main board. Very minor layout changes from 171-5054-02, most evident around the PROM halfway up the heatsink side of the board. Part number is most likely silkscreened on one edge of the parts side of the board as per 171-5054-02, but unfortunately this is usually covered by a sticker giving the assembled game part number.
[![](Pictures/PCB171-5303Top.jpg)](Pictures/PCB171-5303Top.jpg)[![](Pictures/PCB171-5303Bottom.jpg)](Pictures/PCB171-5303Bottom.jpg) | 171-5303 | Main board. Added the 315-5049 tilemap chip. There's a sticker on the PCB to the left of Jamma connector on the parts side of the board, giving the board part number. This is unusual, and it's currently unknown if there is a silkscreened board part number under this sticker which is possibly incorrect, or if it was omitted entirely and the sticker was added to correct this.
[![](Pictures/PCB171-5303-01Top.jpg)](Pictures/PCB171-5303-01Top.jpg)[![](Pictures/PCB171-5303-01Bottom.jpg)](Pictures/PCB171-5303-01Bottom.jpg) | 171-5303-01 | Main board. Very minor layout changes from 171-5303, most evident around IC117. Part number silkscreened to the left of Jamma connector on the parts side of the board.
[![](Pictures/PCB171-5303-02Top.jpg)](Pictures/PCB171-5303-02Top.jpg)[![](Pictures/PCB171-5303-02Bottom.jpg)](Pictures/PCB171-5303-02Bottom.jpg) | 171-5303-02 | Main board. Very minor layout changes from 171-5303-01, most evident around IC70. Part number silkscreened to the left of Jamma connector on the parts side of the board.

</div>

Excluding Super Locomotive, there are three major board variants, with five minor revisions across those lines. Note that the Wonder Boy dipswitch sheet suggests a 171-5154-01 boards existed, but this appears to be a misprint, with "5054" being the intended board number here.

There is not enough information currently available from the various online sources to clearly identify which games were released using which main boards, and the prevalence of conversion kits with daughter boards further complicates matters. With the large number of board revisions, and lack of overall system identity and branding however, it doesn't appear that this was originally intended to be used as a formal general purpose hardware platform. This is further supported by the fact that Star Jacker, the first game to be released on a shared board for this system, had the game-specific 834 full assembly part number silkscreened on the PCB. Later games placed a sticker over this part number, replacing it with their game-specific part number.

It seems clear that there's no distinct System 1 and System 2 hardware platforms, although there is a case to be made that all the various games released with similar and largely compatible hardware form a de-facto system of sorts. It appears likely that one first game was produced with a game-specific main board. To save time and costs however, that board was then re-used for other games. In some cases the board was used in an identical form, but in other cases the main board was revised to extend it slightly, for example to allow larger ROM sizes, or to add additional hardware, but still in a game-specific form. This approach had served Sega well in the past, no doubt reducing the up-front development time and costs for the first title, but with 30+ games released for this line of hardware, more than Sega had ever done before on an arcade platform, this approach would have ended up quite inefficient over time. It appears Sega ultimately found themselves revising the hardware once for approximately every three games they released. It seems highly likely that Sega's experience with this hardware is what lead them to build formal general purpose platforms shortly afterwards. Having a flexible and extensible general purpose base board, with a switchable ROM board, allowed Sega to release many titles on a single hardware platform, reducing R&D costs over the life of the system and simplifying software development, while also making it easier for arcade operators to upgrade to new titles on the same platform.

# Documentation

Part &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Document | Author | Description | Source
--- | --- | --- | --- | ---
| {% include download title="Choplifter Schematics" target="Schematics/Choplifter.pdf" %} | Sega | Complete hardware schematics for Choplifter running on a 171-5303 main board. These are the original schematics from Sega. The schematics are clean, however slightly lower resolution than would be ideal. Some of the smaller labels are a bit hard to make out. | [www.jammarcade.net](http://www.jammarcade.net/schematics/)
420-5110 | {% include download title="Star Jacker Service Manual" target="Manuals/420-5110 - Star Jacker - Service Manual.pdf" %} | Sega | Full service manual for StarJacker, including the board layout and schematics for the 171-5034 main board. | [www.jammarcade.net](http://www.jammarcade.net/schematics/)
28-081300 | {% include download title="Regulus Conversion Kit Technical Manual" target="Manuals/28-081300 - Regulus - Conversion Kit Technical Manual.pdf" %} | Wico | According to the [flyer](https://flyers.arcade-museum.com/flyers_video/sega/16133302.jpg), Regulus was only available as a conversion kit, and Wico was the sole distributor in North America. This is the manual for that conversion kit, including full schematics for the 171-5054 main board. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)
0A64-00300-0000 | {% include download title="Up'n Down - Parts and Operating Manual" target="Manuals/0A64-00300-0000 - Up'n Down - Parts and Operating Manual.pdf" %} | Bally | Licensed reproduction of hardware by Bally/Midway, with full schematics that appear to match 171-5054 main board. | [xmission.com](https://arcarc.xmission.com)
420-5270-91 | {% include download title="Choplifter Instruction Manual" target="Manuals/420-5270-91 - Choplifter - Instruction Manual.pdf" %} | Sega | Choplifter conversion kit manual, with wiring diagram. 171-5303 or 171-5054-03 main board. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)
420-5295 | {% include download title="Wonder Boy Instruction Manual" target="Manuals/420-5295 - Wonder Boy - Instruction Manual.pdf" %} | Sega | Wonder Boy conversion kit manual, with wiring diagram. 171-5034, 171-5054, 171-5054-01, 171-5054-02, 171-5054-03, or 171-5303-02 main board. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)
420-5267 | {% include download title="Shooting Master Owner's Manual" target="Manuals/420-5267 - Shooting Master - Owner's Manual.pdf" %} | Sega | Shooting Master owner's manual, with wiring diagram. 171-5303 main board. | [www.crazykong.com](https://www.crazykong.com/manuals/)
421-5956 | {% include download title="Pitfall Dipswitch and Pinout" target="Manuals/421-5956 - Pitfall - Dipswitch and Pinout.pdf" %} | Sega | Pitfall dipswitch settings and pinout. 171-5034, 171-5054, 171-5054-01 or 171-5054-02 main board. | [xmission.com](https://arcarc.xmission.com)
421-6057 | {% include download title="Heavy Metal Dipswitch and Pinout" target="Manuals/421-6057 - Heavy Metal - Dipswitch and Pinout.pdf" %} | Sega | Heavy Metal dipswitch settings and pinout. 171-5303 or 171-5054-03 main board. | [arcadeinfo.de](http://files.arcadeinfo.de/)
421-6113 | {% include download title="My Hero Dipswitch and Pinout" target="Manuals/421-6113 - My Hero - Dipswitch and Pinout.pdf" %} | Sega | My Hero dipswitch settings and pinout. 171-5034, 171-5054, 171-5054-01, 171-5054-02, or 171-5054-03 main board. | [arcadeinfo.de](http://files.arcadeinfo.de/)
421-6178 | {% include download title="Choplifter Dipswitch and Pinout" target="Manuals/421-6178 - Choplifter - Dipswitch and Pinout.pdf" %} | Sega | Choplifter dipswitch settings and pinout. 171-5303 or 171-5054-03 main board. | [arcadeinfo.de](http://files.arcadeinfo.de/)
421-6285 | {% include download title="Rafflesia Dipswitch and Pinout" target="Manuals/421-6285 - Rafflesia - Dipswitch and Pinout.pdf" %} | Sega | Rafflesia dipswitch settings and pinout. 171-5034, 171-5054, 171-5054-01, 171-5054-02, or 171-5054-03 main board. | [arcadeinfo.de](http://files.arcadeinfo.de/)
421-6307 | {% include download title="Wonder Boy Dipswitch and Pinout" target="Manuals/421-6307 - Wonder Boy - Dipswitch and Pinout.pdf" %} | Sega | Wonder Boy dipswitch settings and pinout. 171-5034, 171-5054, 171-5054-01, 171-5054-02, 171-5054-03, or 171-5303-02 main board. | [arcadeinfo.de](http://files.arcadeinfo.de/)
421-7289 | {% include download title="UFO Senshi Yohko Chan Dipswitch and Pinout" target="Manuals/421-7289 - UFO Senshi Yohko Chan - Dipswitch and Pinout.pdf" %} | Sega | UFO Senshi Yohko Chan dipswitch settings and pinout. 171-5303, 171-5303-01, or 171-5303-02 main board. | [arcadeinfo.de](http://files.arcadeinfo.de/)
CN-112 | {% include download title="Flicky Dipswitch and Pinout" target="Manuals/CN-112 - Flicky - Dipswitch and Pinout.pdf" %} | | Flicky dipswitch settings and pinout. Unofficial third party sheet. | [arcadeinfo.de](http://files.arcadeinfo.de/)
| {% include download title="Wonder Boy II Dipswitch and Pinout" target="Manuals/Wonder Boy II - Dipswitch and Pinout.pdf" %} | | Wonder Boy II dipswitch settings and pinout. Unofficial third party sheet. | [arcadeinfo.de](http://files.arcadeinfo.de/)

# High Resolution Photos
##TODO##

# Miscellaneous Photos
Here's a collection of images of this system that have been posted online from various sources: {% include folder title="Hardware Photos" target="Pictures" %}

<br><br><br><br><br><br>

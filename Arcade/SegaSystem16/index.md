---
layout: default
title: "Sega System 16"
---

# Overview
Refer to [SegaRetro](https://segaretro.org/Sega_System_16) and System16 ([Pre](http://www.system16.com/hardware.php?id=695), [16A](http://www.system16.com/hardware.php?id=700), [16B](http://www.system16.com/hardware.php?id=701)) for a general overview of the Sega System 16 and a list of games.

# System Pictures
{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}
[![](Pictures/ReferenceImage16A11.jpg)](Pictures/ReferenceImage16A11.jpg)
[![](Pictures/ReferenceImage16A20.jpg)](Pictures/ReferenceImage16A20.jpg)
[![](Pictures/ReferenceImage16B.jpg)](Pictures/ReferenceImage16B.jpg)
</div>

# Hardware Revisions
The System 16 has two major hardware variants, known as the System 16A and System 16B. Within the 16A variant there is a major 2.0 board revision, which while looking physically very different is equivalent from a software perspective to the earlier version. There is also a "theoretical" System 16C which added expanded RAM over the System 16B hardware. This hardware never really existed, but it was targeted in a virtual emulation environment for a digital release of "Fantasy Zone II DX" in 2008. This game can be run on System 16B hardware with slight modifications to expand the RAM, however as this is a modification and Sega never produced this hardware themselves, it is not listed here.

Like the System 1, it's likely that Sega hadn't officially named this hardware the "System 16" when the original "System 16A" hardware was released. This name doesn't appear on the game PCB or in any of the manuals and documentation of the time. Unlike the System 1, it's likely this hardware was intended to be used for more than one specific game, however it was still fairly inflexible. The System 16A was re-engineered with the System 16A 2.0 revision however less than a year after the initial release, which for the first time in Sega arcade hardware introduced a separate ROM board. This hardware is frequently mis-identified as "Pre System 16" on many sites, however the names in the official schematics, years of production, PCB part numbers, and nature of design changes all confirm it was in fact a revision of the 16A hardware.

The switch to a separate ROM board largely insulated details of ROM size and layout from the design of the main board, making it easier for Sega to develop new games without revising the basic hardware, and also making upgrades to newer game releases easier. Sega refined this concept with the System 16B hardware, which introduced an I/O expansion port to allow specialized hardware such as trackballs and analog inputs to be attached for games that required them. Dipswitches were added to the ROM boards during the 16B era, making it easier still to reuse existing hardware and reconfigure devices in the field. Sega carried through these design features to most of their following arcade hardware. The System 16B also formally adopted the System 16 branding, with the name "System 16 BType" being silkscreened onto the board. Sega also used formal names for their various hardware platforms from this time forward.

The following PCBs are known to exist for System 16 hardware:

{::options parse_block_html="true" /}
<div class="thumbnails">
{::options parse_block_html="false" /}

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Images&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Part &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Description
--- | --- | ---
[![](Pictures/PCB171-5306Top.jpg)](Pictures/PCB171-5306Top.jpg)[![](Pictures/PCB171-5306Bottom.jpg)](Pictures/PCB171-5306Bottom.jpg) | 171-5306 | System 16A 1.1 Main board No 1, CPU and sound board. Part number etched on solder side of board.
 | 171-5306-01 | System 16A 1.1 Main board No 1 revision. Not identified in pictures, but referenced in Shinobi owner's manual.
[![](Pictures/PCB171-5307Top.jpg)](Pictures/PCB171-5307Top.jpg)[![](Pictures/PCB171-5307Bottom.jpg)](Pictures/PCB171-5307Bottom.jpg) | 171-5307 | System 16A 1.1 Main board No 2, graphics board. Part number etched on solder side of board.
[![](Pictures/PCB171-5335Top.jpg)](Pictures/PCB171-5335Top.jpg)[![](Pictures/PCB171-5335Bottom.jpg)](Pictures/PCB171-5335Bottom.jpg) | 171-5335 | System 16A 2.0 Main board. Part number silkscreened on the parts side to the left of the jamma connector.
[![](Pictures/PCB171-5336Top.jpg)](Pictures/PCB171-5336Top.jpg) | 171-5336 | System 16A 2.0 ROM board
[![](Pictures/PCB171-5357Top.jpg)](Pictures/PCB171-5357Top.jpg)[![](Pictures/PCB171-5357Bottom.jpg)](Pictures/PCB171-5357Bottom.jpg) | 171-5357 | System 16B main board. This appears to be the first and only version of this PCB, used for the life of the system. Part number silkscreened on the parts side to the left of the jamma connector.
[![](Pictures/PCB171-5358Top.jpg)](Pictures/PCB171-5358Top.jpg) | 171-5358 | System 16B ROM board. First version, basic board with roms only. 32K/64K rom support only. Part number silkscreened on parts side right edge of board.
[![](Pictures/PCB171-5521Top.jpg)](Pictures/PCB171-5521Top.jpg) | 171-5521 | System 16B ROM board. 512K/1M/2M rom support. Banking register to access expanded rom capacity. Part number etched on parts side right edge of board.
[![](Pictures/PCB171-5704Top.jpg)](Pictures/PCB171-5704Top.jpg)[![](Pictures/PCB171-5704Bottom.jpg)](Pictures/PCB171-5704Bottom.jpg) | 171-5704 | System 16B ROM board. 512K/1M rom support. Banking register as per 171-5521. More fine grained rom size control over 171-5521, with jumper pins rather than soldered parts for config. Part number etched on parts side right edge of board.
[![](Pictures/PCB171-5797Top.jpg)](Pictures/PCB171-5797Top.jpg) | 171-5797 | System 16B ROM board. 512K/1M(??) rom support. Adds math co-processor, new banking register. Part number etched on parts side right edge of board.
[![](Pictures/PCB171-5468Top.jpg)](Pictures/PCB171-5468Top.jpg) | 171-5468 | Blanking plate. Installed in place of the i8751 protection device on System 16A hardware where no protection device was used.
[![](Pictures/PCB834-6180Top.jpg)](Pictures/PCB834-6180Top.jpg)[![](Pictures/PCB834-6180Bottom.jpg)](Pictures/PCB834-6180Bottom.jpg) | 171-5437/834-6180 | IO Board, Trackball
[![](Pictures/PCB834-6523Top.jpg)](Pictures/PCB834-6523Top.jpg) | 834-6523 | IO Board, 4 player addon.


</div>

# Documentation

Part &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Document | Author | Description | Source
--- | --- | --- | --- | ---
| {% include download title="System 16A 1.1 Schematics (Partial)" target="Schematics/System 16A 1.1 Schematics (Partial).pdf" %} | Sega | Partial hardware schematics for System 16A 1.1 hardware running on 171-5306/171-5307 main boards. These are the original schematics from Sega. The schematics are supposed to contain 13 sheets, 6 sheets for 171-5306 (No.1), 6 sheets for 171-5307 (No.2), and one sheet containing content for both boards. Unfortunately a single sheet (No.2 7/7) is missing, but that sheet appears to have consisted entirely of the connector pinouts for the 171-5307 (No.2) board, the details of which can be entirely reconstructed from the matching connector pinout sheet for 171-5306 (No.1), which is included in the scanned pages. The schematics are clean, however slightly lower resolution than would be ideal, and some trace details are cut off on the edges of some of the pages. | 
| {% include download title="System 16A 2.0 Schematics" target="Schematics/System 16A 2.0 Schematics.pdf" %} | Sega | Complete hardware schematics for System 16A 2.0 hardware running on 171-5335 main board, with 171-5336 ROM board schematics included. These are the original schematics from Sega. | Original Document
| {% include download title="System 16B Schematics" target="Schematics/System 16B Schematics.pdf" %} | Sega | Complete hardware schematics for System 16B hardware running on 171-5357 main board, with 171-5358 ROM board schematics included. These are the original schematics from Sega. <br><br>{% detailsStart Alternate Versions %}{% include download title="System 16B Schematics (Partial) (Annotated)" target="Schematics/System 16B Schematics (Partial) (Annotated).pdf" %}<br>Lower quality schematic scans, missing the last two pages of the main board section. This scan does however include many hand-written annotations in English and Japanese. {% detailsEnd %} | 
420-0005U | {% include download title="Golden Axe Installation and Owner's Manual" target="Manuals/420-0005U - Golden Axe - Installation and Owner's Manual.pdf" %} | Sega | Golden Axe owners manual and installation instructions, including dipswitch settings. System 16B hardware. | 
420-0008U | {% include download title="ESWAT Installation and Owner's Manual" target="Manuals/420-0008U - ESWAT - Installation and Owner's Manual.pdf" %} | Sega | ESWAT owners manual and installation instructions, including dipswitch settings. System 16B hardware. | 
420-5288 | {% include download title="Quartet Owner's Manual" target="Manuals/420-5288 - Quartet - Owner's Manual.pdf" %} | Sega | Quartet owners manual and installation instructions, including dipswitch settings. System 16A 2.0 hardware. | 
999-0055 | {% include download title="Aurail Installation and Owner's Manual" target="Manuals/999-0055 - Aurail - Installation and Owner's Manual.pdf" %} | Sega | Aurail owners manual and installation instructions, including dipswitch settings. System 16B hardware. | 
4201-0001 | {% include download title="Alien Syndrome Owner's Manual" target="Manuals/4201-0001 - Alien Syndrome - Owner's Manual.pdf" %} | Sega | Alien Syndrome owners manual, including dipswitch settings. System 16A 2.0 hardware. | 
4201-0002 | {% include download title="Shinobi Installation and Owner's Manual" target="Manuals/4201-0002 - Shinobi - Installation and Owner's Manual.pdf" %} | Sega | Shinobi owners manual and installation instructions, including dipswitch settings. System 16A 1.1 or 16B hardware. | 
4201-0003 | {% include download title="Altered Beast Installation and Owner's Manual" target="Manuals/4201-0003 - Altered Beast - Installation and Owner's Manual.pdf" %} | Sega | Altered Beast owners manual and installation instructions, including dipswitch settings. System 16B hardware. | 
4201-0004 | {% include download title="Wrestle War Installation and Owner's Manual" target="Manuals/4201-0004 - Wrestle War - Installation and Owner's Manual.pdf" %} | Sega | Wrestle War owners manual and installation instructions, including dipswitch settings. System 16B hardware. | 
 | {% include download title="Quartet 2 Installation Manual" target="Manuals/Quartet 2 - Installation Manual.pdf" %} | Sun | Quartet 2 installation instructions, including dipswitch settings. System 16A 2.0 hardware. | 


# High Resolution Photos
##TODO##

# Miscellaneous Photos
Here's a collection of images of this system that have been posted online from various sources: {% include folder title="Hardware Photos" target="Pictures" %}

<br><br><br><br><br><br>

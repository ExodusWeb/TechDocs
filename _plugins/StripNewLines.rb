module Jekyll
  module Tags
    class StripNewLinesTag < Liquid::Tag

      def initialize(tag_name, markup, tokens)
        super
        @caption = markup
      end

      def render(context)
        site = context.registers[:site]
        converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
        caption = converter.convert(@caption).gsub('\r\n', '').chomp
        "#{caption}"
      end

    end
  end
end

Liquid::Template.register_tag('strip', Jekyll::Tags::StripNewLinesTag)

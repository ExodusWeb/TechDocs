---
layout: default
title: "Sega Part Numbers"
---

# Overview
As you'd expect from a major vendor, Sega used a well-defined system for identifying parts in all hardware they produced. It is noteworthy that a single unified parts system was used between both console and arcade hardware, as well as other physical systems Sega created. With few exceptions, these part numbers were printed, molded, stamped, or labelled on each individual part Sega produced. Although full details on the way these part numbers were encoded were not provided, nor do we have access to the full internal records that Sega would have on each individual part, we attempt here to provide as complete a reference as possible on how to decode Sega part numbers, and to provide lists of some known part numbers.


# Encoding Scheme
Sega used the same part number encoding scheme from 1982 through to their merger with Sammy in 2004. Part numbers take the following general form:  
TTT-NNNNR
- "TTT" is either a three digit number, or a two or three letter prefix code identifying the "type" of the part. Some prefix codes are listed below. Some later part types have used a four digit number here.
- "NNNN" is a four-digit number identifying the specific part. These appear to generally have been sequentially allocated. Later part numbers have extended to five digits where required.
- "R" where present is a single letter postfix indicating the "revision" of the part. If this letter is not present, it indicates the original version of the part, with "A" being the second version, "B" being the third, and so on.

For example, the Sega Mega Drive can contain a VDP graphics chip with the number "315-5313A", where "315" identifies a custom IC, "5313" is the identifying part code, and the "A" suffix indicates this is the second revision of the IC. Some part types also have an additional suffix separated by a third dash, which isn't standardized across part types. This suffix may be optional or required, and it may be numeric or alphabetical, depending on the part.

The following is a partial table listing the more important part type prefixes which relate to the electrical hardware in Sega systems. 

Prefix | Item | Description
--- | --- | ---
171 | Circuit board | Each unique PCB is given a part number with this prefix, even small daughter boards. The part number is either etched or silkscreened onto one side of the board. An additional optional suffix indicating a revision of the board may be included, for example, "171-5054-01" is the second revision of the "5054" pcb. This part number is referred to as the "Bare Board No" in some schematic sheets. When the encoding sceme was adopted in 1981, these numbers appear to have originally began from 0001, but for unknown reasons, numbers were restarted from 5000 at some point in 1982.
315 | Custom IC | Used on all custom ICs produced by or for Sega. Note that 315 codes are also used for programmable devices, such as GALs and PLAs, in this case representing the content the device was programmed with. When the encoding sceme was adopted in 1981, these numbers appear to have originally began from 0001, but for unknown reasons, numbers were restarted from 5000 at some point in 1982.
317 | Protection IC | Used for protection devices, such as protection MCUs, encrypted CPUs, or other self-contained protection units or modules, either battery backed or not. As with GALs and PLAs, two identical protection devices with different programmable content will have different part numbers.
833 or 834 | Full Assembly | In this case, it's not a single part that's being labelled, it's an assembled set of parts. This is most commonly seen for labels placed onto assembled PCB or set of PCBs, with all components and ROMs populated. As the set is fully assembled, these part numbers are game-specific if any programmable devices are on the assembly which contain game-specific code. These part numbers often include a two-digit suffix indicating region or revision. When the encoding sceme was adopted in 1981, these numbers appear to have originally began from 0001, but for unknown reasons, numbers were restarted from 5000 at some point in 1982.
837 | Partial Assembly | An assembled PCB without game-specific elements installed, such as EPROMs. This number was often silkscreened onto PCBs, but unlike the raw "171" PCB number, a sticker may have been placed over the number to indicate revisions that didn't require a new board to be manufactured, such as hand-wired modifications or chip replacements, to upgrade the hardware or fix issues identified after manufacture.
839 | Passthrough Assembly | Used to identify assembled PCBs that route signals only, adding no active components. Filter boards are the most common example. A 171 PCB number is also provided for the bare PCB itself. On systems where filter boards didn't populate all the connectors for games that didn't need them, unique part numbers would exist for the same board where different combinations of connectors and parts are populated.
EPR | EPROM | UV erasable programmable read-only memory ICs
PR | PROM | Early one-time programmable read-only memory ICs
MPR | Mask ROM | Factory programmed ROM device, where the data is determined by the masks used when the surface of the IC is etched during manufacturing.
OPR | One-Time Programmable ROM | Modern version of PROM device under the newer CMOS process
FPR | Flash Programmable ROM | More modern surface-mount flash EEPROM devices

There are many more part codes that exist, as parts right down to individual nuts, washers, labels, and buttons all have codes. Service manuals are the best source of information for these additional part codes, and lists of these part types have not been compiled here.


# History
The way Sega encodes part numbers is based on the scheme Gremlin Industries used to encode their part numbers, which they acquired in 1978. Although the encoding scheme is the same, Sega used different prefix codes to ones already in use by Gremlin to avoid confusion. Prior to acquiring Gremlin Industries, Sega used a part number encoding scheme in the following form for most parts:  
- XXXXX-P

Where "XXXXX" was a 5-digit part number. This didn't allow the type of the part to be determined from the part alone, with items such as circuit boards and heat sinks both having part numbers mixed together. Sega did however have different ways of identifying code on programmable devices, using the following encoding schemes for PROMs, EPROMs, and Mask ROMs respectively:  
- PR-XXX
- EPR-XXX
- MPR-XXX

Where "XXX" was a sequentially allocated number without leading zeros. Sega didn't change their part number scheme immediately after acquiring Gremlin, but appears to have switched over to the new encoding scheme at some point during 1981, with "Turbo" being the earliest located example of a game using the new scheme. Hardware manufactured by Gremlin Industries continued to use their separate original encoding, through until mid 1983 when Gremlin Industries was sold off to Bally Manufacturing. When Sega switched their part number encoding scheme, they continued using their old convention for identifying PROM, EPROM, and Mask ROM devices, now with leading zeros up to four digits, and mixed it with a new set of three digit numeric prefix codes derived from Gremlin's part type numbers. In later systems where Sega used One-Time Programmable ROMs, the OPR prefix was adopted in keeping with the naming convention used for earlier ROM types.


# Dates and serial numbers
In addition to part numbers, earlier arcade boards produced by Sega also often had a date of manufacture stamped somewhere on the edge of the main PCB in black lettering, usually in the form YYMM or YYMMDD, as well as a five digit serial number. Later games used a sticker for serial numbers, gave more flexibility for placement, as well as being more useful when production was performed at a larger scale, and the PCBs themselves may have been manufactured significantly before assembly took place. These stickers used varying conventions through the years, with some including a date code as part of the serial number, and some being just a raw serial number. Consoles produced by Sega had their own serial number systems which varied over time and between regions. No attempt will be made here to further describe the varying serial number and date schemes used across Sega hardware, however additional information may be provided in the sections on individual systems.


# Parts harmonization
During the key years of 1982-1992, Sega produced a wide range of hardware which share common ICs. This wasn't common before this time period, when systems were often built from discrete logic circuits, or afterwards, when systems had longer lifespans and relied more heavily on large custom ASIC devices. The following spreadsheet attempts to highlight which ICs are used across which systems:<br>
[Sega Hardware 1982-1992 - Parts Harmonization](https://docs.google.com/spreadsheets/d/1fGrZejCZ8GmTZo1zzvIX3ysYuzx2ZlckDJWftBL8q2s)


# PCB Part List (171-XXXX) ##TODO##

Code | Part
--- | ---
171-5357 | System 16B CPU Board
171-5358 | System 16B ROM Board Type 1
171-5521 | System 16B ROM Board Type 2
171-5704 | System 16B ROM Board Type 3


# Protection Device Part List (317-XXXX) ##TODO##

Code | System | Part
--- | --- | ---
317-0018 | System 16A | Alex Kidd FD1089A
317-0033 | System 16B | Alien Syndrome FD1089A
317-0034 | OutRun | Super HangOn FD1089B
317-0059 | System 16B | Ace Attacker FD1094
317-0068 | System 16B | Jyuohki FD1094
317-0069 | System 16B | Altered Beast FD1094
317-0115 | System 16B | Bay Route FD1094
317-0129 | System 16B | ESwat FD1094A
317-0168 | System 16B | Aurail FD1089B
317-0181A | System 16B | Cotton FD1094A

<br><br><br><br><br><br>
